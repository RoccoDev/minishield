package dev.rocco.mods.minishield;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.registry.CommandRegistry;
import net.fabricmc.loader.FabricLoader;
import net.minecraft.client.MinecraftClient;
import net.minecraft.server.command.CommandManager;

import java.io.*;
import java.util.Properties;

public class ShieldMod implements ModInitializer {

    public static float SHIELD_SIZE;
    public static int PERCENT;

    @Override
    public void onInitialize() {
        File configFile = new File(FabricLoader.INSTANCE.getConfigDirectory(), "minishield.properties");
        Properties props = new Properties();
        if(!configFile.exists()) {
            try {
                configFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            SHIELD_SIZE = 0.03125f;
            PERCENT = 50;

            save();
        }
        else {
            try(InputStream is = new FileInputStream(configFile)) {
                props.load(is);

                PERCENT = Integer.parseInt(props.getProperty("scale", "50"));
                SHIELD_SIZE = PERCENT * 0.0625f / 100f;
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        CommandRegistry.INSTANCE.register(false, dispatcher ->
                dispatcher.register(CommandManager.literal("shield")
                        .executes(ctx -> {
                            MinecraftClient.getInstance().openScreen(new ConfigGui());
                            return 1;
                        })));
    }

    public static void save() {
        File configFile = new File(FabricLoader.INSTANCE.getConfigDirectory(), "minishield.properties");
        Properties props = new Properties();
        props.put("scale", Integer.toString(PERCENT));

        try(OutputStream os = new FileOutputStream(configFile)) {
            props.store(os, null);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
