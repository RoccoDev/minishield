package dev.rocco.mods.minishield.mixins;

import net.minecraft.client.model.Cuboid;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(Cuboid.class)
public interface CuboidMixin {
    @Accessor
    void setCompiled(boolean compiled);
}
