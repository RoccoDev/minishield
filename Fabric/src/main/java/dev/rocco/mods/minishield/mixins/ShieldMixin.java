package dev.rocco.mods.minishield.mixins;

import dev.rocco.mods.minishield.ShieldMod;
import net.minecraft.client.model.Cuboid;
import net.minecraft.client.render.entity.model.ShieldEntityModel;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(ShieldEntityModel.class)
public abstract class ShieldMixin {

    @Shadow
    private Cuboid field_3550;

    @Shadow
    private Cuboid field_3551;

    private float lastSize;

    /**
     * The original method is just these two lines with "0.0625F" as the size.
     * @author RoccoDev
     * @reason We also need to recompile the cuboids so that we can apply the changes in real time.
     */
    @Overwrite
    public void renderItem() {
        if(lastSize != ShieldMod.SHIELD_SIZE) {
            lastSize = ShieldMod.SHIELD_SIZE;
            ((CuboidMixin)field_3550).setCompiled(false);
            ((CuboidMixin)field_3551).setCompiled(false);
        }
        field_3550.render(ShieldMod.SHIELD_SIZE);
        field_3551.render(ShieldMod.SHIELD_SIZE);
    }
}
