package dev.rocco.mods.minishield;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.AbstractSlider;
import net.minecraft.util.text.StringTextComponent;

public class ConfigGui extends Screen {

    protected ConfigGui() {
        super(new StringTextComponent("Shield Settings"));
    }

    @Override
    protected void init() {
        AbstractSlider slider = new AbstractSlider(width / 2 - 60, height / 2 - 10, 120,
                20, ShieldMod.PERCENT / 100D) {
            @Override
            protected void updateMessage() {
                setMessage("Shield Size: " + ShieldMod.PERCENT + "%");
            }

            @Override
            protected void applyValue() {
                ShieldMod.PERCENT = (int) (value * 100D);
                ShieldMod.SHIELD_SIZE = ShieldMod.PERCENT * 0.0625f / 100f;
            }
        };
        slider.setMessage("Shield Size: " + ShieldMod.PERCENT + "%");
        addButton(slider);
    }


    @Override
    public void render(int int_1, int int_2, float float_1) {
        drawCenteredString(Minecraft.getInstance().fontRenderer, "Note: You need to restart your game for the changes to take effect.",
                width / 2, height / 2 - 50, 0xffffffff);
        super.render(int_1, int_2, float_1);
    }

    @Override
    public void onClose() {
        ShieldMod.save();
        super.onClose();
    }
}
