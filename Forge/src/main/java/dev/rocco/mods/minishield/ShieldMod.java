package dev.rocco.mods.minishield;

import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;

import java.io.*;
import java.util.Properties;

@Mod("minishield")
public class ShieldMod {
    public static float SHIELD_SIZE;
    public static int PERCENT;

    public ShieldMod() {
        MinecraftForge.EVENT_BUS.addListener(this::serverStarting);

        File configFile = new File(Minecraft.getInstance().gameDir, "config/minishield.properties");
        Properties props = new Properties();
        if(!configFile.exists()) {
            try {
                configFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            SHIELD_SIZE = 0.03125f;
            PERCENT = 50;

            save();
        }
        else {
            try(InputStream is = new FileInputStream(configFile)) {
                props.load(is);

                PERCENT = Integer.parseInt(props.getProperty("scale", "50"));
                SHIELD_SIZE = PERCENT * 0.0625f / 100f;
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void serverStarting(FMLServerStartingEvent event) {
        CommandDispatcher<CommandSource> disp = event.getCommandDispatcher();
        disp.register(Commands.literal("shield").executes((p_198622_0_) -> {
            Minecraft.getInstance().displayGuiScreen(new ConfigGui());
            return 1;
        }));
    }

    public static void save() {
        File configFile = new File(Minecraft.getInstance().gameDir, "config/minishield.properties");
        Properties props = new Properties();
        props.put("scale", Integer.toString(PERCENT));

        try(OutputStream os = new FileOutputStream(configFile)) {
            props.store(os, null);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
