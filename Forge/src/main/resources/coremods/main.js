function initializeCoreMod() {
    var Opcodes = Java.type("org.objectweb.asm.Opcodes");

    var transform = function(method) {
        return function(){
            var arrayLength = method.instructions.size();
            for (var i = 0; i < arrayLength; ++i) {
                var instruction = method.instructions.get(i);
                if (instruction.getOpcode() == Opcodes.LDC) {
                    var Field = Java.type("org.objectweb.asm.tree.FieldInsnNode");
                    var loadField = new Field(Opcodes.GETSTATIC, "dev/rocco/mods/minishield/ShieldMod", "SHIELD_SIZE", "F");
                    method.instructions.insertBefore(instruction, loadField);
                    method.instructions.remove(instruction);
                }
            }
            return method;
        };
    };

    var transformAll = function(method){
        var f1 = transform(method);
        if (f1 === false) return false;

        f1();
        return true;
    };

    return {
        "minishield": {
            "target": {
                "type": "METHOD",
                "class": "net.minecraft.client.renderer.entity.model.ShieldModel",
                "methodName": "func_187062_a",
                "methodDesc": "()V"
            },
            "transformer": function(methodNode){
                transformAll(methodNode);
                return methodNode;
            }
        }
    };
}
